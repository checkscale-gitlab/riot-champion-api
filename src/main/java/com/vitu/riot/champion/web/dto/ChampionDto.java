package com.vitu.riot.champion.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChampionDto {

    private String name;
    private String description;
    private String role;
    private String difficulty;
    private List<SkinDto> skins;
    private String region;
    private List<SkillDto> skills;

}
