package com.vitu.riot.champion.cucumber.datatables;

import com.vitu.riot.champion.web.dto.ChampionDto;
import com.vitu.riot.champion.web.dto.SkillDto;
import com.vitu.riot.champion.web.dto.SkinDto;
import io.cucumber.java.DataTableType;

import java.util.Map;


public class CreateChampionDataTable {

    @DataTableType
    public ChampionDto createChampion(Map<String, String> map) {
        return ChampionDto.builder()
                .description(map.get("description"))
                .name(map.get("name"))
                .region(map.get("region"))
                .role(map.get("role"))
                .difficulty(map.get("difficulty"))
                .build();
    }

    @DataTableType
    public SkillDto createSkill(Map<String, String> map) {
        return SkillDto.builder()
                .description(map.get("description"))
                .name(map.get("name"))
                .build();
    }

    @DataTableType
    public SkinDto createSkin(Map<String, String> map) {
        return SkinDto.builder()
                .theme(map.get("theme"))
                .value(map.get("value"))
                .name(map.get("name"))
                .build();
    }

}
